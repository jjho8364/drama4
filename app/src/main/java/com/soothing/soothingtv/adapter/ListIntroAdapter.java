package com.soothing.soothingtv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.soothing.soothingtv.R;
import com.soothing.soothingtv.items.ListIntroItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListIntroAdapter extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ListIntroItem> ListViewArr = null;
    private int nListCnt = 0;

    public ListIntroAdapter(ArrayList<ListIntroItem> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public ImageView kind;
        public TextView intro;
        public TextView btn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.item_list_intro, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.kind= (ImageView)convertView.findViewById(R.id.tv_item_kind);
            viewHolder.intro = (TextView)convertView.findViewById(R.id.tv_item_intro);
            viewHolder.btn = (TextView)convertView.findViewById(R.id.tv_btn);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        ListIntroItem item = ListViewArr.get(position);

        Picasso.with(parent.getContext()).load(item.getImgUrl()).into(holder.kind);
        holder.intro.setText(item.getIntro());

        if(item.getLinUrl().contains("drive.google.com")){
            holder.btn.setText("구글드라이브에서 다운");
        } else {
            holder.btn.setText("앱스토어로 이동");
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }

}
