package com.soothing.soothingtv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.soothing.soothingtv.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class VideoJSActivity extends Activity {
    private String TAG = " VideoJSActivity - ";
    private ProgressDialog mProgressDialog;
    private String videoUrl;
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private WebviewPlayerActivity.myWebChromeClient mWebChromeClient;
    private WebviewPlayerActivity.myWebViewClient mWebViewClient;

    private String html = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_videojs);

        videoUrl = getIntent().getStringExtra("videoUrl");

        webView = (WebView) findViewById(R.id.webView);


    }

    /*public class GetImageUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(VideoJSActivity.this);
            mProgressDialog.setTitle("이미지를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document btnDoc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();
                btnDoc = Jsoup.connect(baseUrl).timeout(15000).get();

                Element iframe = doc.select("#iframe").first();
                String iframeSrc = iframe.attr("src");

                if(iframeSrc != null) {
                    doc = Jsoup.connect(iframeSrc).timeout(20000).get();
                }

                Elements lists = doc.select(".listType.view li img");

                html += "<html>";
                html += "   <head><style>img{display: inline;height: auto;max-width: 100%;}</style></head>";
                html += "   <body>";
                html += "       <div>";

                for(int i=0 ; i<lists.size() ; i++) {
                    String imgUrl = lists.get(i).attr("src");
                    itemArr.add(new ViewActivityItem(imgUrl));

                    html += "           <img src='" + imgUrl + "'>";


                    if(i != lists.size()-1) {
                        html += "        <br /><hr><br />";
                    }

                }

                html += "       <div>";
                html += "   </body>";
                html += "</html>";

                // preeps and next eps
                Elements btns = btnDoc.select(".btnC a");

                for(int i=0 ; i<btns.size() ; i++) {
                    String epsUrl = firstUrl + btns.get(i).attr("href");
                    String text = btns.get(i).text();

                    if(text.equals("이전 보기")) preUrl = epsUrl;
                    if(text.equals("다음 보기")) nextUrl = epsUrl;
                }

                // history
                Elements navs = btnDoc.select("#nav ul li");
                nowTitle = navs.get(navs.size()-1).text();

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pushSfArr();
            sfArr[0] = nowTitle + "`" + baseUrl;
            setSfArr();

            if(ViewActivity.this != null){
                webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
            }

            mProgressDialog.dismiss();
        }
    }*/


}
