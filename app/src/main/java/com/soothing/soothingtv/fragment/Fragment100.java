package com.soothing.soothingtv.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.soothing.soothingtv.R;
import com.soothing.soothingtv.adapter.ListIntroAdapter;
import com.soothing.soothingtv.items.ListIntroItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment100
extends Fragment implements View.OnClickListener, LocationListener {
private final String TAG = " Fragment100 - ";
private ProgressDialog mProgressDialog;
private ListView listView;
private GetListView getListView = null;
private String baseUrl = "";
private ArrayList<ListIntroItem> itemArr;

private int adsCnt = 0;
private InterstitialAd interstitialAd;
        AdRequest adRequest;
        SharedPreferences pref;

@Nullable
@Override
public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_start));
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        getListView = new GetListView();
        getListView.execute();

        return view;
        }
public class GetListView extends AsyncTask<Void, Void, Void> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        itemArr = new ArrayList<ListIntroItem>();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        Document doc = null;

        try {
            Log.d(TAG, "baseUrl : " + baseUrl);
            doc = Jsoup.connect(baseUrl).timeout(10000).get();

            Elements lists = doc.select(".intro_app_new");

            for(int i=0 ; i<lists.size() ; i++){
                String intro = lists.get(i).attr("alt").split(",")[0];
                String imgUrl = lists.get(i).attr("src");
                String linkUrl = lists.get(i).attr("alt").split(",")[1];

                Log.d(TAG, "intro : " + intro);
                Log.d(TAG, "imgUrl : " + imgUrl);
                Log.d(TAG, "linkUrl : " + linkUrl);

                ListIntroItem listIntroItem = new ListIntroItem(imgUrl, intro, linkUrl);
                itemArr.add(listIntroItem);
            }

        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(getActivity() != null){
            listView.setAdapter(new ListIntroAdapter(itemArr));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(adsCnt == 1){
                        adsCnt++;
                        SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                        editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                        editor.commit(); //완료한다.
                        interstitialAd.show();
                    } else {
                        String tempUrl = itemArr.get(position).getLinUrl();

                        if(tempUrl.contains("drive.google.com")){
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempUrl));
                            startActivity(intent);
                        } else {
                            Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                            marketLaunch2.setData(Uri.parse(tempUrl));
                            startActivity(marketLaunch2);
                        }
                    }
                }
            });
        }

        mProgressDialog.dismiss();
    }
}

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }
}